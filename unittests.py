#!/usr/bin/env python3

import unittest
from empleado import Empleado
from empleado import Jefe 

class TestEmpleado(unittest.TestCase):

    def test_construir(self):
        e1 = Empleado("nombre",5000)
        self.assertEqual(e1.nomina, 5000)

    def test_impuestos(self):
        e1 = Empleado("nombre",5000)
        self.assertEqual(e1.calcula_impuestos(), 1500)
   
    def test_str(self):
        e1 = Empleado("pepe",50000) 
        self.assertEqual("El empleado pepe debe pagar 15000.00", e1.__str__())

class TestJefe(unittest.TestCase):

    def test_jefe(self):
        e1 = Jefe("Paco",30000,2000)
        self.assertEqual(e1.prima,2000)

    def test_impuestos(self):
        e1 = Jefe ("Paco",30000,2000)
        self.assertEqual(e1.calcula_impuestos(), 9600.00)
    
    def test_str(self):
        e1 = Jefe("Paco",30000,2000)
        self.assertEqual(e1.__str__(),"El jefe Paco debe pagar 9600.00")

if __name__ == "__main__":
    unittest.main()